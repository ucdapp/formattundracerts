#How-to Use
Converts certificate information returned by TundraIoT UI in .CSV file into config file data.

### Run
```
node index.js [CSVFILE.csv] [FILE_PREFIX]
```

### Required Arguments
* none 

### Optional Arguments
* CSVFILE.csv = path to CSV file to parse.
* FILE_PREFIX = name of each file created will start with this.