const fs = require('fs');
// const csv = require('csv');
const csv = require('csvtojson');

module.exports = async (filename) => {
  try {
    // csvtojson
    const csvArray = await csv().fromFile(filename);
    // console.log(csvArray);
    // csv
    // let content = await new Promise((resolve, reject) => {
    //   fs.readFile(fileInfo.path, function(err, data) {
    //     if (err) {
    //       return reject(err);
    //     }
    //     return resolve(data.toString());
    //   });
    // });

    // let csvArray = await new Promise((resolve, reject) => {
    //   csv.parse(content, { delimiter: ',' }, (err, data) => {
    //     if (err) {
    //       return reject(err);
    //     } else {
    //       return resolve(data);
    //     }
    //   });
    // });

    return csvArray;
  } catch (err) {
    console.log(err);
  }
};

//test();
