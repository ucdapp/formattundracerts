const fs = require('fs');
const csv = require('./csv');
var mkdirp = require('async-mkdirp');

let go = async () => {
  let filePrefix = 'CUSTOMERNAME_SERVERNAME'; // ex. 'cvilux_dev', 'nextlink_prod', etc.

  var toProcess = [
    {
      address: '00-00-00-00-00-01',
      identity: '0123456789abcdef0000000000000000',
      keys: {
        certificate:
          '-----BEGIN CERTIFICATE-----\nMIIDWTC2\n0vxfH998frt\n-----END CERTIFICATE-----\n',

        public: '-----BEGIN PUBLIC KEY-----\nMIIBIjAN\nIQIDAQAB\n-----END PUBLIC KEY-----\n',

        private: '-----BEGIN RSA PRIVATE KEY-----\nMIIE\n2GGNOOu\n-----END RSA PRIVATE KEY-----\n'
      }
    }
  ];

  // Get CSV from command line arg
  let cvsMode = false;

  console.log('See README.MD for how to use.');

  process.argv.forEach(async function(val, index, array) {
    console.log(index);
    if (index <= 1) {
      return;
    }

    console.log('checking arg ' + val);
    let splits = val.split('.');
    if (splits && splits.length > 0 && splits[splits.length - 1] === 'csv') {
      // CVS FILE, try.
      cvsMode = val;
      console.log('CSV file: ' + val);
    } else {
      console.log('Prefix: ' + val);
      filePrefix = val;
    }
  });

  if (cvsMode != false) {
    // console.log('CVS MODE!');
    let csvresults = await csv(cvsMode);

    toProcess = csvresults;

    // Remove any with errors
    for (var i = toProcess.length - 1; i >= 0; i -= 1) {
      // console.log(i);
      // console.log('toProcess[i]');
      // console.log(toProcess[i]);
      if (toProcess[i].result !== '') {
        console.log('Unable to process row ' + i + ': ' + " row.result must be '' ");
        toProcess.splice(i, 1); // remove this element
      }
    }
  }

  // If valid, replace data  in previously defined vars
  for (var i = 0; i < toProcess.length; i += 1) {
    // Convert newlines
    console.log('toProcess[i]');
    console.log(toProcess[i]);

    var last_device_identity = toProcess[i].identity;
    var last_device_cert = toProcess[i].keys.certificate.replace(/\\n/g, '\n');
    var last_device_pub_key = toProcess[i].keys.public.replace(/\\n/g, '\n');
    var last_device_private_key = toProcess[i].keys.private.replace(/\\n/g, '\n');

    let dir = './output/' + filePrefix + '/device_certs/';
    await mkdirp(dir);

    // Save to file
    let publicName = filePrefix + '_' + toProcess[i].identity + '.public.pem.key';
    fs.writeFileSync(dir + publicName, last_device_pub_key);

    let privateName = filePrefix + '_' + toProcess[i].identity + '.private.pem.key';
    fs.writeFileSync(dir + privateName, last_device_pub_key);

    let certName = filePrefix + '_' + toProcess[i].identity + '.pem.crt';
    fs.writeFileSync(dir + certName, last_device_cert);

    let configFileContents = `module.exports.deviceId = '${toProcess[i].identity}';\n`;
    configFileContents += `module.exports.address = '${toProcess[i].address}' ;\n`;
    configFileContents += `module.exports.clientCert = './device_certs/${certName}';\n`;
    configFileContents += `module.exports.privateKey = './device_certs/${certName}';\n`;

    let configDir = './output/' + filePrefix + '/device_config/';
    await mkdirp(configDir);
    fs.writeFileSync(configDir + 'index.js', configFileContents);

    console.log('Files created for: ' + filePrefix + '_' + toProcess[i].identity);
  }
};

go();
